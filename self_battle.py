import random
import numpy as np
import tensorflow as tf
import crow.src as crow
import pyothello.src as pyothello
import zerobra.src as zerobra

def zerobra_routine(board, my_stone, network, temperature_parameter):
    legal_coords_2d = pyothello.current_legal_coords(board)
    legal_coords_1d = pyothello.current_legal_1d_coords(board)
    input_data_list = pyothello.board_to_binary(board, my_stone)
    input_data = np.array(input_data_list).reshape(1, *zerobra.INPUT_SHAPE)
    output = network.run_output(input_data)[0]
    legal_coord_only_output = [value for i, value in enumerate(output) if i in legal_coords_1d]
    if (temperature_parameter != 0):
        index = crow.boltzmann_random(np.array(legal_coord_only_output), temperature_parameter)
    else:
        index = np.argmax(legal_coord_only_output)
    return legal_coords_2d[index], input_data_list, legal_coords_1d[index]

def play_one_game(network1, network2, U):
    zerobra1_stone_color = random.choice([pyothello.BLACK, pyothello.WHITE])
    board, current_turn, is_game_end = pyothello.INITIAL_BOARD, pyothello.BLACK, False

    zerobra1_one_game_input_data_list = []
    zerobra1_one_game_coord_1d_memory = []

    zerobra2_one_game_input_data_list = []
    zerobra2_one_game_coord_1d_memory = []

    turn_count = 1

    while not is_game_end:
        if zerobra1_stone_color == current_turn:
            network = network1
            one_game_input_data_list = zerobra1_one_game_input_data_list
            one_game_coord_1d_memory = zerobra1_one_game_coord_1d_memory
        else:
            network = network2
            one_game_input_data_list = zerobra2_one_game_input_data_list
            one_game_coord_1d_memory = zerobra2_one_game_coord_1d_memory

        if turn_count <= U:
            temperature_parameter = 1
        else:
            temperature_parameter = 0

        coord, input_data_list, coord_1d = zerobra_routine(board, current_turn, network, temperature_parameter)

        turn_count += 1

        one_game_input_data_list.append(input_data_list)
        one_game_coord_1d_memory.append(coord_1d)

        board, current_turn, is_game_end = pyothello.put_stone(board, coord, current_turn)

    winner = pyothello.winner(board)
    if winner == zerobra1_stone_color:
        zerobra1_win_result = 1
    elif winner == pyothello.OPPONENT_STONE[zerobra1_stone_color]:
        zerobra1_win_result = -1
    else:
        zerobra1_win_result = 0

    return zerobra1_win_result, zerobra1_one_game_input_data_list, zerobra1_one_game_coord_1d_memory, \
           zerobra2_one_game_input_data_list, zerobra2_one_game_coord_1d_memory,

def play_game(game_num, network1, network2):
    zerobra1_win_count = 0
    zerobra2_win_count = 0

    zerobra1_input_data_list_memory = []
    zerobra1_coord_1d_memory = []

    zerobra2_input_data_list_memory = []
    zerobra2_coord_1d_memory = []
    U_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

    for _ in range(game_num):
        zerobra1_win_result, zerobra1_one_game_input_data_list, zerobra1_one_game_coord_1d_memory, \
        zerobra2_one_game_input_data_list, zerobra2_one_game_coord_1d_memory = \
            play_one_game(network1, network2, random.choice(U_list))

        zerobra1_input_data_list_memory += zerobra1_one_game_input_data_list
        zerobra1_coord_1d_memory += zerobra1_one_game_coord_1d_memory
        zerobra2_input_data_list_memory += zerobra2_one_game_input_data_list
        zerobra2_coord_1d_memory += zerobra2_one_game_coord_1d_memory

        if zerobra1_win_result == 1:
            zerobra1_win_count += 1
        elif zerobra1_win_result == -1:
            zerobra2_win_count += 1

    if zerobra1_win_count > zerobra2_win_count:
        zerobra1_label_data_list = crow.batch_one_hot_label_list(zerobra.OUTPUT_SIZE, zerobra1_coord_1d_memory)
        zerobra2_label_data_list = crow.batch_reverse_one_hot_label_list(zerobra.OUTPUT_SIZE, zerobra2_coord_1d_memory)
    elif zerobra1_win_count < zerobra2_win_count:
        zerobra1_label_data_list = crow.batch_reverse_one_hot_label_list(zerobra.OUTPUT_SIZE, zerobra1_coord_1d_memory)
        zerobra2_label_data_list = crow.batch_one_hot_label_list(zerobra.OUTPUT_SIZE, zerobra2_coord_1d_memory)
    else:
        zerobra1_label_data_list = []
        zerobra2_label_data_list = []

    return zerobra1_input_data_list_memory + zerobra2_input_data_list_memory, \
           zerobra1_label_data_list + zerobra2_label_data_list, zerobra1_win_count, zerobra2_win_count

def play_one_game_vs_random(network):
    zerobra_stone_color = random.choice([pyothello.BLACK, pyothello.WHITE])
    board, current_turn, is_game_end = pyothello.INITIAL_BOARD, pyothello.BLACK, False

    while not is_game_end:
        if current_turn == zerobra_stone_color:
            coord, _, _, = zerobra_routine(board, current_turn, network, 0)
        else:
            coord = pyothello.random_choice_legal_coord(board)
        board, current_turn, is_game_end = pyothello.put_stone(board, coord, current_turn)
    return pyothello.winner(board) == zerobra_stone_color

def play_game_vs_random(game_num, network):
    result = 0
    for _ in range(game_num):
        if play_one_game_vs_random(network):
            result += 1
    return result

def main():
    session = tf.Session()
    network1 = zerobra.Network(session)
    network2 = zerobra.Network(session)
    session.run(tf.global_variables_initializer())

    iter_num = 1280
    one_cycle = 64
    mini_batch_learning_iter_num = 32
    mini_batch_size = one_cycle
    test_game_num = 16

    keep_list = [0.9, 1.0]

    for _ in range(iter_num):
        input_data_list_memory, label_data_list_memory, zerobra1_win_count, zerobra2_win_count = \
            play_game(one_cycle, network1, network2)

        if zerobra1_win_count != zerobra2_win_count:
            print("zerobra1_win_count_vs_self = ", zerobra1_win_count, "zerobra2_win_count_self = ", zerobra2_win_count)
            batch_input_data = np.array(input_data_list_memory).reshape(len(input_data_list_memory), *zerobra.INPUT_SHAPE)
            batch_label_data = np.array(label_data_list_memory)

            network1.mini_batch_learning(mini_batch_learning_iter_num, mini_batch_size, batch_input_data, batch_label_data,
                                         random.choice(keep_list), random.choice(keep_list))

            network2.mini_batch_learning(mini_batch_learning_iter_num, mini_batch_size, batch_input_data, batch_label_data,
                                         random.choice(keep_list), random.choice(keep_list))

            zerobra1_win_count_vs_random = play_game_vs_random(test_game_num, network1)
            zerobra2_win_count_vs_random = play_game_vs_random(test_game_num, network2)
            print("zerobra1_vs_random_win_rate = " + str((zerobra1_win_count_vs_random / test_game_num) * 100) + "%")
            print("zerobra2_vs_random_win_rate = " + str((zerobra2_win_count_vs_random / test_game_num) * 100) + "%")

if __name__ == "__main__":
    main()
