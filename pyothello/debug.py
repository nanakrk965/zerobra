import unittest
import src as pyothello

class Test(unittest.TestCase):
    def test_stone_line(self):
        result = pyothello.stone_line(
            pyothello.INITIAL_BOARD, (7, 4), (-1, 0),
        )
        expected = [0, 0, 3, 2, 1, 0, 0, 0]
        self.assertEqual(result, expected)

    def test_stone_line_reverse_num(self):
        stone_line = [0, 2, 1, 0, 0]
        result = pyothello.stone_line_reverse_num(stone_line, 1)
        expected = 1
        self.assertEqual(result, expected)

        stone_line = [0, 1, 1, 1, 2, 0]
        result = pyothello.stone_line_reverse_num(stone_line, 2)
        expected = 3
        self.assertEqual(result, expected)

        stone_line = [0, 2, 2, 0, 1]
        result = pyothello.stone_line_reverse_num(stone_line, 1)
        expected = 0
        self.assertEqual(result, expected)

        stone_line = [0, 1, 1, 1, 1]
        result = pyothello.stone_line_reverse_num(stone_line, 2)
        expected = 0
        self.assertEqual(result, expected)

    def test_reverse_coords(self):
        board = [
            [0, 0, 0, 0, 0, 0, 0, 0],
            [1, 0, 0, 1, 0, 0, 1, 0],
            [0, 2, 0, 2, 0, 2, 0, 0],
            [0, 0, 2, 2, 2, 0, 0, 0],
            [1, 2, 2, 3, 2, 2, 1, 0],
            [0, 0, 2, 2, 2, 0, 0, 0],
            [0, 2, 0, 2, 0, 2, 0, 0],
            [1, 0, 0, 1, 0, 0, 1, 0],
        ]

        result = pyothello.reverse_coords(board, (4, 3), 1)
        expected = [
            (3, 3), (2, 3),
            (5, 3), (6, 3),
            (4, 2), (4, 1),
            (4, 4), (4, 5),
            (3, 2), (2, 1),
            (3, 4), (2, 5),
            (5, 2), (6, 1),
            (5, 4), (6, 5),
        ]
        self.assertEqual(result, expected)

    def test_current_legal_coords(self):
        result = pyothello.current_legal_coords(pyothello.INITIAL_BOARD)
        expected = [(2, 3), (3, 2), (4, 5), (5, 4)]
        self.assertEqual(result, expected)

    def test_next_legal_coords(self):
        board = [
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 2, 1, 0, 0, 0],
            [0, 0, 0, 1, 1, 1, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0],
        ]
        result = pyothello.next_legal_coords(board, 2)
        expected = [(3, 5), (5, 3), (5, 5)]
        self.assertEqual(result, expected)

    def test_turn_pass(self):
        coords = [(4, 5), (5, 5), (2, 3), (4, 6),
                  (4, 7), (3, 7), (6, 5), (5, 7)]
        board, current_turn = pyothello.INITIAL_BOARD, pyothello.BLACK

        for i, coord in enumerate(coords):
            board, current_turn, _ = pyothello.put_stone(board, coord, current_turn)

            if i == 7:
                self.assertEqual(current_turn, 2)
            elif i%2 == 0:
                self.assertEqual(current_turn, 2)
            elif i%2 == 1:
                self.assertEqual(current_turn, 1)

    def test_is_game_end(self):
        coords = [(4, 5), (5, 3), (4, 2), (3, 5), (2, 4), (5, 5),
                  (4, 6), (5, 4), (6, 4)]
        board, current_turn, is_game_end = pyothello.INITIAL_BOARD, pyothello.BLACK, False
        for i, coord in enumerate(coords):
            board, current_turn, is_game_end = pyothello.put_stone(board, coord, current_turn)
            if i == 8:
                self.assertTrue(is_game_end)
                break
            self.assertFalse(is_game_end)

if __name__ == "__main__":
    unittest.main()
