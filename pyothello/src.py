import random

EMPTY = 0
BLACK = 1
WHITE = 2
LEGAL = 3

OPPONENT_STONE = {1:2, 2:1}

ALL_DIRECTIONS = (
    (-1, 0), (1, 0), (0, -1), (0, 1),
    (-1, -1), (-1, 1), (1, -1), (1, 1),
)

ALL_COORDS = (
    (0, 0), (0, 1), (0, 2), (0, 3), (0, 4), (0, 5), (0, 6), (0, 7),
    (1, 0), (1, 1), (1, 2), (1, 3), (1, 4), (1, 5), (1, 6), (1, 7),
    (2, 0), (2, 1), (2, 2), (2, 3), (2, 4), (2, 5), (2, 6), (2, 7),
    (3, 0), (3, 1), (3, 2), (3, 3), (3, 4), (3, 5), (3, 6), (3, 7),
    (4, 0), (4, 1), (4, 2), (4, 3), (4, 4), (4, 5), (4, 6), (4, 7),
    (5, 0), (5, 1), (5, 2), (5, 3), (5, 4), (5, 5), (5, 6), (5, 7),
    (6, 0), (6, 1), (6, 2), (6, 3), (6, 4), (6, 5), (6, 6), (6, 7),
    (7, 0), (7, 1), (7, 2), (7, 3), (7, 4), (7, 5), (7, 6), (7, 7),
)

INITIAL_BOARD = [
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 3, 0, 0, 0, 0],
    [0, 0, 3, 2, 1, 0, 0, 0],
    [0, 0, 0, 1, 2, 3, 0, 0],
    [0, 0, 0, 0, 3, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0, 0, 0, 0],
]

def stone_line(board, coord, direction):
    result = []
    row, column = coord

    while not (row in [-1, 8] or column in [-1, 8]):
        result.append(board[row][column])
        row += direction[0]
        column += direction[1]
    return result

def stone_line_my_stone_first_index(drop_stone_line, my_stone):
    for i, stone in enumerate(drop_stone_line):
        if stone == my_stone:
            return i
    assert False

def stone_line_reverse_num(stone_line, my_stone):
    oppoent_stone = OPPONENT_STONE[my_stone]

    if len(stone_line) < 3:
        return 0
    elif stone_line[0] not in [EMPTY, LEGAL]:
        return 0
    elif stone_line[1] != oppoent_stone:
        return 0

    drop_stone_line = stone_line[2:]
    ex_op_st_stone_line = [stone for stone in drop_stone_line if stone != oppoent_stone]

    if len(ex_op_st_stone_line) == 0:
        return 0
    elif ex_op_st_stone_line[0] in [EMPTY, LEGAL]:
        return 0
    return stone_line_my_stone_first_index(drop_stone_line, my_stone) + 1

def append_each_direction_reverse_coords(board, coord, my_stone, direction, result):
    sl = stone_line(board, coord, direction)
    reverse_num = stone_line_reverse_num(sl, my_stone)

    row, column = coord
    for _ in range(reverse_num):
        row += direction[0]
        column += direction[1]
        result.append((row, column))

def reverse_coords(board, coord, my_stone):
    result = []
    for direction in ALL_DIRECTIONS:
        append_each_direction_reverse_coords(board, coord, my_stone, direction, result)
    return result

def copy_board(board):
    return [row_line_board.copy() for row_line_board in board].copy()

def black_stone_count(board):
    return sum([row_line_board.count(BLACK) for row_line_board in board])

def white_stone_count(board):
    return sum([row_line_board.count(WHITE) for row_line_board in board])

def current_legal_coords(board):
    result = [coord for coord in ALL_COORDS \
              if board[coord[0]][coord[1]] == LEGAL]
    return result

def next_legal_coords(board, my_stone):
    return [coord for coord in ALL_COORDS if len(reverse_coords(board, coord, my_stone)) != 0]

def update_next_legal_coord(__board, my_stone):
    board = copy_board(__board)

    for coord in current_legal_coords(board):
        board[coord[0]][coord[1]] = EMPTY

    for coord in next_legal_coords(board, my_stone):
        board[coord[0]][coord[1]] = LEGAL
    return board

def reverse(__board, coord, my_stone):
    board = copy_board(__board)
    r_coords = reverse_coords(board, coord, my_stone)
    assert len(r_coords) != 0

    for r_coord in r_coords:
        board[r_coord[0]][r_coord[1]] = my_stone
    return board

def is_game_end(board):
    black_legal_coord_num = len(next_legal_coords(board, BLACK))
    white_legal_coord_num = len(next_legal_coords(board, WHITE))
    return black_legal_coord_num == 0 and white_legal_coord_num == 0

def put_stone(__board, coord, my_stone):
    assert __board[coord[0]][coord[1]] == LEGAL

    board = reverse(__board, coord, my_stone)
    board[coord[0]][coord[1]] = my_stone

    if is_game_end(board):
        return board, my_stone, True

    oppoent_stone = OPPONENT_STONE[my_stone]

    if len(next_legal_coords(board, oppoent_stone)) != 0:
        board = update_next_legal_coord(board, oppoent_stone)
        return board, oppoent_stone, False
    else:
        board = update_next_legal_coord(board, my_stone)
        return board, my_stone, False

DRAW = -1

def winner(board):
    assert is_game_end(board)
    black_stone_num = black_stone_count(board)
    white_stone_num = white_stone_count(board)

    if black_stone_num > white_stone_num:
        return BLACK
    elif black_stone_num < white_stone_num:
        return WHITE
    else:
        return DRAW

def stone_to_str_stone(stone):
    if stone == EMPTY:
        return "□"
    elif stone == BLACK:
        return "○"
    elif stone == WHITE:
        return "●"
    elif stone == LEGAL:
        return "×"

def row_line_board_to_str_row_line_board(row_line_board):
    return "".join([stone_to_str_stone(stone) for stone in row_line_board])

def print_board(board):
    for row_line_board in board:
        print(row_line_board_to_str_row_line_board(row_line_board))

def random_choice_legal_coord(board):
    return random.choice(current_legal_coords(board))

COORD_2D_TO_COORD_1D = {coord:i for i, coord in enumerate(ALL_COORDS)}

def current_legal_1d_coords(board):
    coords_2d = current_legal_coords(board)
    return [COORD_2D_TO_COORD_1D[coord_2d] for coord_2d in coords_2d]

def each_info_to_binary(board, arg_stone):
    result = [[1 if stone == arg_stone else 0 for stone in row_line_board]
              for row_line_board in board]
    return result

def board_to_binary(board, my_stone):
    if my_stone == BLACK:
        result = [each_info_to_binary(board, BLACK),
                  each_info_to_binary(board, WHITE),
                  each_info_to_binary(board, LEGAL)]
    else:
        result = [each_info_to_binary(board, WHITE),
                  each_info_to_binary(board, BLACK),
                  each_info_to_binary(board, LEGAL)]
    return result

def exmple():
    print_board(INITIAL_BOARD)
    board, current_turn, is_game_end = INITIAL_BOARD, BLACK, False
    while not is_game_end:
        coord = random_choice_legal_coord(board)
        board, current_turn, is_game_end = put_stone(board, coord, current_turn)
        print_board(board)
        print("coord:", coord, "current_turn:", current_turn)
        if input("エンターで次に進みます qで終了\n") == "q":
            break

if __name__ == "__main__":
    for binary in each_info_to_binary(INITIAL_BOARD, 2):
        print(binary)
