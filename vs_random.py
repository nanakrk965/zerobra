import random
import numpy as np
import tensorflow as tf
import crow.src as crow
import pyothello.src as pyothello
import zerobra.src as zerobra

def zerobra_routine(board, my_stone, network):
    input_data_list = pyothello.board_to_binary(board, my_stone)
    input_data = np.array(input_data_list).reshape(1, *zerobra.INPUT_SHAPE)
    output = network.run_output(input_data)[0]
    legal_coords_2d = pyothello.current_legal_coords(board)
    legal_coords_1d = pyothello.current_legal_1d_coords(board)
    legal_coord_only_output = [value for i, value in enumerate(output) if i in legal_coords_1d]
    index = np.argmax(legal_coord_only_output)
    return legal_coords_2d[index], input_data_list, legal_coords_1d[index]

def play_one_game(network):
    zerobra_stone_color = random.choice([pyothello.BLACK, pyothello.WHITE])
    board, current_turn, is_game_end = pyothello.INITIAL_BOARD, pyothello.BLACK, False

    one_game_input_data_list = []
    one_game_coord_1d_memory = []

    while not is_game_end:
        if zerobra_stone_color == current_turn:
            coord, input_data_list, coord_1d = zerobra_routine(board, zerobra_stone_color, network)
            one_game_input_data_list.append(input_data_list)
            one_game_coord_1d_memory.append(coord_1d)
        else:
            coord = pyothello.random_choice_legal_coord(board)
        board, current_turn, is_game_end = pyothello.put_stone(board, coord, current_turn)

    is_zerobra_win = pyothello.winner(board) == zerobra_stone_color

    if is_zerobra_win:
        one_game_label_data_list = crow.batch_one_hot_label_list(zerobra.OUTPUT_SIZE, one_game_coord_1d_memory)
    else:
        one_game_label_data_list = crow.batch_reverse_one_hot_label_list(zerobra.OUTPUT_SIZE, one_game_coord_1d_memory)
    return is_zerobra_win, one_game_input_data_list, one_game_label_data_list

def play_game(game_num, network, online_learning):
    zerobra_win_count = 0
    input_data_list_memory = []
    label_data_list_memory = []
    for _ in range(game_num):
        is_zerobra_win, one_game_input_data_list, one_game_label_data_list = play_one_game(network)

        if online_learning:
            input_data = np.array(one_game_input_data_list).reshape(len(one_game_input_data_list), *zerobra.INPUT_SHAPE)
            network.run_optimizer(input_data, np.array(one_game_label_data_list), 1.0, 1.0)

        input_data_list_memory += one_game_input_data_list
        label_data_list_memory += one_game_label_data_list

        if is_zerobra_win:
            zerobra_win_count += 1
    return zerobra_win_count, input_data_list_memory, label_data_list_memory

def main():
    session = tf.Session()
    network = zerobra.Network(session)
    session.run(tf.global_variables_initializer())

    iter_num = 128
    one_cycle = 128
    mini_batch_learning_iter_num = 64
    mini_batch_size = 128

    keep_list = [0.9, 1.0]

    for _ in range(iter_num):
        zerobra_win_count, input_data_list_memory, label_data_list_memory = play_game(one_cycle, network, False)
        batch_input_data = np.array(input_data_list_memory).reshape(len(input_data_list_memory), *zerobra.INPUT_SHAPE)
        batch_label_data = np.array(label_data_list_memory)
        print(str(one_cycle) + "戦" + str(zerobra_win_count) + "勝",
              "勝率:" + str((zerobra_win_count / one_cycle) * 100) + "%")
        network.mini_batch_learning(mini_batch_learning_iter_num, mini_batch_size, batch_input_data, batch_label_data,
                                    random.choice(keep_list), random.choice(keep_list))

if __name__ == "__main__":
    main()
