import random

import numpy as np
import tensorflow as tf

import crow.src as crow
import pyothello.src as pyothello

INPUT_SHAPE = 8, 8, 3
OUTPUT_SIZE = 64

class Network:
    def __init__(self, session):
        self.session = session

        self.input_holder = tf.placeholder(tf.float32, [None, *INPUT_SHAPE])
        self.label_holder = tf.placeholder(tf.float32, [None, OUTPUT_SIZE])
        self.cnn_keep_holder = tf.placeholder(tf.float32)
        self.affine_keep_holder = tf.placeholder(tf.float32)
        self.is_training_holder = tf.placeholder(tf.bool)

        stddev = 0.01

        filter1_size = 128
        filter1 = crow.normal_variable([3, 3, INPUT_SHAPE[2], filter1_size], stddev=stddev)
        alpha1 = crow.zeros_variable([filter1_size])

        filter2_size = 128
        filter2 = crow.normal_variable([3, 3, filter1_size, filter2_size], stddev=stddev)
        alpha2 = crow.zeros_variable([filter2_size])

        filter3_size = 128
        filter3 = crow.normal_variable([3, 3, filter2_size, filter3_size], stddev=stddev)
        alpha3 = crow.zeros_variable([filter3_size])

        filter4_size = 128
        filter4 = crow.normal_variable([3, 3, filter3_size, filter4_size], stddev=stddev)
        alpha4 = crow.zeros_variable([filter4_size])

        filter5_size = 128
        filter5 = crow.normal_variable([3, 3, filter4_size, filter5_size], stddev=stddev)
        alpha5 = crow.zeros_variable([filter5_size])

        filter6_size = 128
        filter6 = crow.normal_variable([3, 3, filter5_size, filter6_size], stddev=stddev)
        alpha6 = crow.zeros_variable([filter6_size])

        filter7_size = 128
        filter7 = crow.normal_variable([3, 3, filter6_size, filter7_size], stddev=stddev)
        alpha7 = crow.zeros_variable([filter7_size])

        filter8_size = 128
        filter8 = crow.normal_variable([3, 3, filter7_size, filter8_size], stddev=stddev)
        alpha8 = crow.zeros_variable([filter8_size])

        filter9_size = 128
        filter9 = crow.normal_variable([3, 3, filter8_size, filter9_size], stddev=stddev)
        alpha9 = crow.zeros_variable([filter9_size])

        filter10_size = 128
        filter10 = crow.normal_variable([3, 3, filter9_size, filter10_size], stddev=stddev)
        alpha10 = crow.zeros_variable([filter10_size])

        filter11_size = 128
        filter11 = crow.normal_variable([3, 3, filter10_size, filter11_size], stddev=stddev)
        alpha11 = crow.zeros_variable([filter11_size])

        filter12_size = 128
        filter12 = crow.normal_variable([3, 3, filter11_size, filter12_size], stddev=stddev)
        alpha12 = crow.zeros_variable([filter12_size])

        affine1_size = 128
        w1 = crow.normal_variable([INPUT_SHAPE[0] * INPUT_SHAPE[1] * filter12_size, affine1_size], stddev=stddev)
        affine_alpha1 = crow.zeros_variable([affine1_size])

        w2 = crow.normal_variable([affine1_size, OUTPUT_SIZE], stddev=stddev)
        affine_alpha2 = crow.zeros_variable([OUTPUT_SIZE])

        cnn_layers1 = crow.conv_BN_prelu_dropout(self.input_holder, filter1, self.is_training_holder, alpha1, self.cnn_keep_holder)
        cnn_layers2 = crow.conv_BN_prelu_dropout(cnn_layers1, filter2, self.is_training_holder, alpha2, self.cnn_keep_holder)
        cnn_layers3 = crow.conv_BN_prelu_dropout(cnn_layers2, filter3, self.is_training_holder, alpha3, self.cnn_keep_holder)
        cnn_layers4 = crow.conv_BN_prelu_dropout(cnn_layers3, filter4, self.is_training_holder, alpha4, self.cnn_keep_holder)
        cnn_layers5 = crow.conv_BN_prelu_dropout(cnn_layers4, filter5, self.is_training_holder, alpha5, self.cnn_keep_holder)
        cnn_layers6 = crow.conv_BN_prelu_dropout(cnn_layers5, filter6, self.is_training_holder, alpha6, self.cnn_keep_holder)
        cnn_layers7 = crow.conv_BN_prelu_dropout(cnn_layers6, filter7, self.is_training_holder, alpha7, self.cnn_keep_holder)
        cnn_layers8 = crow.conv_BN_prelu_dropout(cnn_layers7, filter8, self.is_training_holder, alpha8, self.cnn_keep_holder)
        cnn_layers9 = crow.conv_BN_prelu_dropout(cnn_layers8, filter9, self.is_training_holder, alpha9, self.cnn_keep_holder)
        cnn_layers10 = crow.conv_BN_prelu_dropout(cnn_layers9, filter10, self.is_training_holder, alpha10, self.cnn_keep_holder)
        cnn_layers11 = crow.conv_BN_prelu_dropout(cnn_layers10, filter11, self.is_training_holder, alpha11, self.cnn_keep_holder)
        cnn_layers12 = crow.conv_BN_prelu_dropout(cnn_layers11, filter12, self.is_training_holder, alpha12, self.cnn_keep_holder)

        flatten = crow.flatten(cnn_layers12)
        affine_layers1 = crow.matmul_BN_prelu_dropout(flatten, w1, self.is_training_holder, affine_alpha1, self.affine_keep_holder)
        last_matmul = tf.matmul(affine_layers1, w2)
        last_prelu = crow.prelu(last_matmul, affine_alpha2)
        self.output = tf.nn.softmax(last_prelu)

        l2_decay = tf.nn.l2_loss(filter1) + tf.nn.l2_loss(filter2) + tf.nn.l2_loss(filter3) \
                 + tf.nn.l2_loss(filter4) + tf.nn.l2_loss(filter5) + tf.nn.l2_loss(filter6) \
                 + tf.nn.l2_loss(filter7) + tf.nn.l2_loss(filter8) + tf.nn.l2_loss(filter9) \
                 + tf.nn.l2_loss(filter10) + tf.nn.l2_loss(filter11) + tf.nn.l2_loss(filter12) \
                 + tf.nn.l2_loss(w1) + tf.nn.l2_loss(w2)

        self.loss = crow.cross_entropy(self.output, self.label_holder) + (l2_decay * 0.0001)

        update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)[-26:]
        assert len(update_ops) == 26

        with tf.control_dependencies(update_ops):
            self.optimizer = crow.AdaBoundOptimizer().minimize(self.loss)

        self.saver = tf.train.Saver()

    def run_output(self, input_data):
        return self.session.run(self.output, feed_dict={self.input_holder:input_data,
                                                        self.cnn_keep_holder:1.0,
                                                        self.affine_keep_holder:1.0,
                                                        self.is_training_holder:False})

    def run_optimizer(self, input_data, label_data, cnn_keep, affine_keep):
        self.session.run(self.optimizer, feed_dict={self.input_holder:input_data,
                                                    self.label_holder:label_data,
                                                    self.cnn_keep_holder:cnn_keep,
                                                    self.affine_keep_holder:affine_keep,
                                                    self.is_training_holder:True})

    def mini_batch_learning(self, iter_num, mini_batch_size, batch_input_data, batch_label_data, cnn_keep, affine_keep):
        batch_range = len(batch_input_data)
        assert batch_range == len(batch_label_data)

        for _ in range(iter_num):
            indices = np.random.choice(batch_range, mini_batch_size)
            self.run_optimizer(batch_input_data[indices], batch_label_data[indices], cnn_keep, affine_keep)
